%global pkg_prefix kiran

Name:		kiran-themes
Version:	0.8.1
Release:	3.kb1
Summary:	Standard themes for Kiran desktop.

Group:		Unspecified
License:	LGPL-2.0-or-later
URL:		http://www.kylinos.com.cn

Requires:	kiran-icon-theme
Requires:	kiran-gtk-theme
Requires:	kiran-wallpapers
BuildArch:	noarch

%description
The kiran-themes package contains the standard theme for the Kiran
desktop, which provides default appearance for cursors, desktop background,
window borders and GTK+ applications.

%prep

%build

%install

%files

%changelog
* Mon Aug 08 2022 luoqing <luoqing@kylinsec.com.cn> - 0.8.1-3.kb1
- KYOS-F: Modify license.

* Mon Aug 01 2022 yuanxing <yuanxing@kylinos.com.cn> - 0.8.1-2.kb1
- KYOS-F: remove %{dist} in Release

* Thu May 28 2020 songchuanfei <songchuanfei@kylinos.com.cn> - 0.8.1-1.kb1
- KYOS-F: Make it an dummy package

* Mon Apr 13 2020 songchuanfei <songchuanfei@kylinos.com.cn> - 0.8.0-5.kb1
- KYOS-B: gtk3: fix treeview row jump when row separators are drawn, Related #25344
- gtk3: set label font weight to bold for selected tabs
- gtk3: Update styles for handles and separators in mate panel
- gtk3: Add border to distinguish mate-panel and others

* Wed Apr 08 2020 songchuanfei <songchuanfei@kylinos.com.cn> - 0.8.0-4.kb1
- KYOS-B: gtk3: change textview background color to fix textview invisible problem
- KYOS-B: gtk3: fix toolbar buttons border when window is inactive
- Remove duplicate gtk-3.20 style files

* Wed Apr 08 2020 songchuanfei <songchuanfei@kylinos.com.cn> - 0.8.0-3.kb1
- KYOS-B: gtk2: Fix theme error messages
- KYOS-B: gtk3: Fix icon jump when hover check buttons
- KYOS-F: gtk3: Update styles for mate-panel 

* Fri Jan 17 2020 liaowei <liaowei@kylinos.com.cn> - 0.8.0-2.kb2
- KYOS-B: replace default wallpapers

* Mon Dec 30 2019 songchuanfei <songchuanfei@kylinos.com.cn> - 0.8.0-2.kb1
- KYOS-B: rebuild for KY3.3-6

* Fri Dec 27 2019 songchuanfei <songchuanfei@kylinos.com.cn> - 0.8.0-2
- gtk2: Move window title to center of title frame
- gtk2/gtk3: use same background color for both titlebar and menubars
- gtk2: add style for mate-panel
- gtk3: use system font settings
- gtk3: update selected tab background color to white
- gtk2: use different color for prelight and active states of panel buttons
- gtk2: Update seperator style and add top line for panel
- gtk2: decrease font size for clock applet on panel
- Add new wallpapers
- Remove depends like kiran-session and adobe fonts

* Thu Apr 11 2019 Luo Yong <luoyong@kylinos.com.cn> - 0.8.0-1
- KYOS-F: redraw theme style and change icon style.
  Related: #16892

* Wed Jan 23 2019 Luo Yong <luoyong@kylinos.com.cn> - 0.6.0-3
- Modify: optimize blackcrystal theme.

* Thu Nov 29 2018 kojibuilder <kojibuilder@kylinos.com.cn> - 0.6.0-2.kb10
- kylin builder

* Thu Nov 29 2018 kojibuilder <kojibuilder@kylinos.com.cn> - 0.6.0-2.kb9
- kylin builder

* Thu Nov 29 2018 kojibuilder <kojibuilder@kylinos.com.cn> - 0.6.0-2.kb8
- kylin builder

* Thu Nov 29 2018 kojibuilder <kojibuilder@kylinos.com.cn> - 0.6.0-2.kb7
- kylin builder

* Thu Oct 18 2018 Luo Yong <luoyong@kylinos.com.cn> - 0.6.0-2.ky3.kb6
- Modify: add showdesktop style class.
  Related: #12484

* Thu Sep 20 2018 Luo Yong <luoyong@kylinos.com.cn> - 0.6.0-2.ky3.kb5
- Modify: change some icons.

* Tue Sep 04 2018 kojibuilder <kojibuilder@kylinos.com.cn> - 0.6.0-2.ky3.kb4
- kylin builder

* Fri Jun 29 2018 Luo Yong <luoyong@kylinos.com.cn> - 0.6.0-2.ky3.kb3
- Modify: create panel popupmenu style class.
- Modify: change default theme blackcrystal.
  Related: #10401-13

* Fri Apr 20 2018 Luo Yong <luoyong@kylinos.com.cn> - 0.6.0-2.ky3.kb2
- Add: add icon category-symblic.
  Related: #12484

* Fri Apr 13 2018 Luo Yong <luoyong@kylinos.com.cn> - 0.6.0-2.ky3.kb1
- Modify: change network and audio icons.

* Fri Apr 13 2018 Luo Yong <luoyong@kylinos.com.cn> - 0.6.0-2
- Modify: change gnome shell color for each theme.
- Add: add pictures for HTML.
  Related: #12484

* Sat Jan 6 2018 Luo Yong <luoyong@kylinos.com.cn> - 0.6.0-1
- Initial version.
  Related: #11234
